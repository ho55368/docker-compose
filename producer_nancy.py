import confluent_kafka

from loguru import logger
import time
import json
import requests
import pydantic

TOPIC = "realtime-weather-data"
#TOPIC = ""


class WeatherDataModel(pydantic.BaseModel):
    pass


def get_weather_data() -> dict:
    url = "https://api.openweathermap.org/data/2.5/weather?lat=51.5073219&lon=-0.1276474&appid=3b2ebdc259c724bb93180d5b3ad683bf"
    payload = {}
    headers = {}
    response = requests.request("GET", url, headers=headers, data=payload)
    result = response.json()
    logger.info(result)
    return result


def init_kafka():
    return confluent_kafka.Producer(
        {
            #"bootstrap.servers": "localhost:9092",
            "bootstrap.servers": "b-2.globalkafkadev.lx6pkj.c3.kafka.ap-east-1.amazonaws.com:9096",
            #"bootstrap.servers": "b-1.test3.z6tgp2.c3.kafka.ap-southeast-1.amazonaws.com:9096",
            # "security.protocol": "PLAINTEXT",
            # "sasl.mechanisms": "PLAIN",
            "security.protocol": "SASL_SSL",
            "sasl.mechanisms": "SCRAM-SHA-512",  
            "sasl.username": "kafka-admin",
            "sasl.password": "xkQJJKiNXZa7NW4ucDHp",
        }
    )


def ack(err, msg):
    if err is not None:
        logger.error(err)
    else:
        logger.info(
            f"Message sent to topic: {msg.topic()}, partition: {msg.partition()} with value: {msg.value()}"
        )


def main():

    kafka_pro_client = init_kafka()
    while True:
#        data = get_weather_data()
        data = {'coord': {'lon': -0.1337, 'lat': 51.51}, 'weather': [{'id': 701, 'main': 'Mist', 'description': 'mist', 'icon': '50n'}], 'base': 'stations', 'main': {'temp': 
284.8, 'feels_like': 284.53, 'temp_min': 283.29, 'temp_max': 285.64, 'pressure': 1007, 'humidity': 96}, 'visibility': 4600, 'wind': {'speed': 1.54, 'deg': 80}, 'clouds': {'all': 100}, 'dt': 1682648879, 'sys': {'type': 2, 'id': 2075535, 'country': 'GB', 'sunrise': 1682656723, 'sunset': 1682709431}, 'timezone': 3600, 'id': 2643743, 'name': 'London', 'cod': 200}
        #print(data)
        kafka_pro_client.produce(TOPIC, json.dumps(data).encode("utf-8"), callback=ack)
        kafka_pro_client.poll(1)
        time.sleep(5)


if __name__ == "__main__":
    main()
