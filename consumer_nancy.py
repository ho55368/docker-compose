import confluent_kafka
from loguru import logger


TOPIC = "realtime-weather-data"
GROUP_ID = "xyz"


def init_kafka():
    consumer = confluent_kafka.Consumer(
        {
            #"bootstrap.servers": "localhost:9092",
            "bootstrap.servers": "b-2.globalkafkadev.lx6pkj.c3.kafka.ap-east-1.amazonaws.com:9096",
            #"bootstrap.servers": "b-1.test3.z6tgp2.c3.kafka.ap-southeast-1.amazonaws.com:9096",
            "group.id": GROUP_ID,
            "auto.offset.reset": "earliest",
            "enable.auto.commit": "false",
            #"security.protocol": "PLAINTEXT",
            #"sasl.mechanisms": "PLAIN",
            "security.protocol": "SASL_SSL",
            "sasl.mechanisms": "SCRAM-SHA-512",            
            "sasl.username": "kafka-admin",
            "sasl.password": "xkQJJKiNXZa7NW4ucDHp",
                                
        }
    )
    consumer.subscribe([TOPIC])
    return consumer


def main():
    consumer = init_kafka()
    while True:
        msg = consumer.poll(timeout=1.0)
        if msg is None:
            continue
        logger.info(f"Received message: {msg.value()}")


if __name__ == "__main__":
    main()
